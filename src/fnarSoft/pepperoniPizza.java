package fnarSoft;

public class pepperoniPizza extends pizza  {
	
	public float price ;
	public boolean available ;
	public pizzaType pizzatype=pizzaType.PEPPERONI;
	
	pepperoniPizza(float price,boolean available){
		this.price = price;
		this.available = available;
	}
	public void prepared() {
		System.out.print("prepared pepperoni pizza !");
		
	}

	public void cut() {
		System.out.print("prepared pepperoni pizza !");
		
	}

	public void boxed() {
		System.out.print("prepared pepperoni pizza !");
		
	}
	public boolean checkPizzaAvailability() {
		return this.available;
	}
	public void setPizzaAvailability(boolean available) {
		this.available=available;
	}

	
	public float getPrice() {
		return this.price;
	}
}
