package fnarSoft;

public abstract class pizza {
	public abstract void prepared();
	public abstract void cut();
	public abstract void boxed();
	public abstract boolean checkPizzaAvailability();
	public abstract void setPizzaAvailability(boolean available);
	public abstract float getPrice();
}
