package fnarSoft;

public class veggiePizza extends pizza {
	public  float price;
	public boolean available;
	public pizzaType pizzatype=pizzaType.VEGGIE;
	veggiePizza( float price,boolean available)
	{
		this.price = price;
		this.available = available;
	}
	public void prepared() {
		System.out.print("veggie pepperoni pizza !");
		
	}
	public void cut() {
		System.out.print("veggie pepperoni pizza !");
		
	}

	public void boxed() {
		System.out.print("veggie pepperoni pizza !");
		
	}
	
	public boolean checkPizzaAvailability() {
		return this.available;
	}
	
	public void setPizzaAvailability(boolean available) {
		this.available=available;
	}
	
	public float getPrice() {
		return this.price;
	}
	

}
